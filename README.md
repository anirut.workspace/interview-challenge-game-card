# Interview-challenge-game-card

## Disclaimer
เนื่องจากเวลาในการจัดทำของตัวผมเองมีจำกัด ทำให้หลายอย่างอาจจะ implement ไม่ครบถ้วน ต้องขออภัยล่วงหน้าด้วยครับ เนื่องจากไม่ให้ทาง Bluepi เสียเวลาและโอกาส จึงไม่ขอเลื่อนเวลาในการส่ง เนื่องจากตัวผมเองยังไม่แน่ใจเรื่องเวลาเช่นกัน

ขอบคุณครับ

## How to setup
 
1. install docker and docker-compose
2. chmod +x scripts/start_local
3. ./script/start_local
4. then put your ip4 address
5. ! FUN !
6. open http://localhost
   
---
เจอ error นี้ ไม่ต้องตกใจนะครับ เป็น error ที่ยอมรับได้
```   
panic: sql: no rows in result set

goroutine 1 [running]:
main.main()
	/app/update_global_score.go:70 +0x827
mock up cronjob update global score
Successfully database is connected!
panic: sql: no rows in result set
```
---

## Documentations
---
### Usecase Diagram
![alt usecase diagram](./usecase.png)

### Architecture Diagram
![alt arch diagram](./arch.png)

### Sequence Diagrams

Enter Website
```mermaid
sequenceDiagram
    participant Client
    participant Frontend
    participant GameCardProcessorService
    participant Redis
    participant RabbitMQ
    participant GameCardSetterService
    participant Postgres
    
    Client->>Frontend: Enter Username
    Frontend->>GameCardProcessorService: Username
    GameCardProcessorService ->> Redis: GET GAME-{Username}
    alt Found in redis
    Redis ->> GameCardProcessorService: data [{"game_id":"xasdas","score":10}]
    else Not Found
        GameCardProcessorService ->> Postgres: Query By Username
        alt Found user
        Postgres ->> GameCardProcessorService: data [{"game_id":"xasdas","score":10}]
        else Not found
            GameCardProcessorService ->> GameCardProcessorService: make default user score
        end
    end
    GameCardProcessorService ->> Redis: GET GAME-GLOBAL-SCORE
    alt Found in redis
        Redis ->> GameCardProcessorService: 10
    else Not Found
        GameCardProcessorService ->> Postgres: Query Best Score
        Postgres ->> GameCardProcessorService: 10
    end
    GameCardProcessorService ->> Frontend: return {"my_score":"0","best_score":"10"}
```

---

Start New Game
```mermaid
    sequenceDiagram
    participant Client
    participant Frontend
    participant GameCardProcessorService
    participant Redis
    participant RabbitMQ
    participant GameCardSetterService
    participant Postgres

    Client ->> Frontend: click New Game
    Frontend ->> GameCardProcessorService: username
    GameCardProcessorService ->> GameCardProcessorService: generate game_id
    GameCardProcessorService ->> GameCardProcessorService: random card
    GameCardProcessorService ->> Redis: set GAME-{game_id} {"username":"",game_id:"",cards:[]}
    GameCardProcessorService -->> RabbitMQ:  publish {"username":"",game_id:"",cards:[]} into new_game queue
    RabbitMQ -->> GameCardSetterService: consume
    GameCardSetterService ->> Postgres: insert {"username":"",game_id:"",cards:[]}
    GameCardProcessorService ->> Frontend: {"username":"",game_id:"",cards:[]}
```
---
Play Game 
```mermaid
sequenceDiagram
    participant Client
    participant Frontend
    participant GameCardProcessorService
    participant Redis
    participant RabbitMQ
    participant GameCardSetterService
    participant Postgres

    Client ->> Frontend: click card number 1
    Frontend ->> GameCardProcessorService: request card {"game_id":"",username:"","card":2,"previous": -1}
    GameCardProcessorService ->> Redis: GET GAME-{game_id}
    Redis ->> GameCardProcessorService:return {"username":"",game_id:"",cards:[]}
    GameCardProcessorService ->> Redis: GET GAME-HISTORY-{game_id}
    Redis ->> GameCardProcessorService: return {"game_id":"","history":[]}
    alt previous equal -1
        GameCardProcessorService ->> GameCardProcessorService: append history
        GameCardProcessorService ->> Redis: SET GAME-HISTORY-{game_id}
        GameCardProcessorService ->> Frontend: return {"card":2,"value":3,"opened_card":[],"round":len(history)}
    else matched card
        GameCardProcessorService ->> GameCardProcessorService: append history
        GameCardProcessorService ->> Redis: SET GAME-HISTORY-{game_id}
        GameCardProcessorService ->> Frontend: return {"card":2,"value":3,"opened_card":[1,3],"round":len(history)}

    end
    GameCardProcessorService ->> Frontend: flip card
```
