import { useRouter } from "next/router";
import React from "react";

const withUsername = (WrapperComponent: React.ComponentType) => () => {
  // prevent rendering on server side
  if (!process.browser) {
    return <></>;
  }
  const router = useRouter();
  const username = localStorage.getItem("username");
  if (username) {
    return <WrapperComponent></WrapperComponent>;
  }
  router.replace("/");
  return <></>;
};

export default withUsername;
