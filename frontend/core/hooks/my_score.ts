import React, { useEffect } from "react";
import { CardServiceImpl } from "../services/card_service";

const useMyBestScore = () => {
  const [score, setScore] = React.useState<number>(0);
  const service = new CardServiceImpl();

  async function updateUserScore() {
    const username = localStorage.getItem("username");
    const sc = await service.getMyScore(username as string);
    setScore(sc)
  }

  useEffect(() => {
    updateUserScore()
  }, []);

  return [score,updateUserScore]
};


export default useMyBestScore