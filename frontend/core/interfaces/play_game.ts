export interface IPlayGameResponse {
  isCardMatched: boolean;
  round: number;
  currentCardValue: number;
  openedCards: Array<{
    cardIndex: number;
    cardValue: number;
  }>;
}
