import "isomorphic-fetch";
import { IPlayGameResponse } from "../interfaces/play_game";
import { IStartGameResponse } from "../interfaces/start_game";

const API =
  process.env.NODE_ENV == "production"
    ? "/api"
    : "http://localhost/api";

export abstract class CardService {
  abstract startGame(username: string): Promise<IStartGameResponse>;
  abstract playGame(
    username: string,
    gameID: string,
    currentIndex: number,
    previousIndex: number
  ): Promise<IPlayGameResponse>;
  abstract getGlobalScore(): Promise<number>;
  abstract getMyScore(username: string): Promise<number>;
}

export class CardServiceImpl implements CardService {
  async getGlobalScore(): Promise<number> {
    const data = await this._fetch<{ score: number }>("GET", "/score");
    return data.score;
  }

  async getMyScore(username: string): Promise<number> {
    const data = await this._fetch<{ score: number }>(
      "GET",
      `/score/${username}/best`
    );
    return data.score;
  }

  startGame = async (username: string): Promise<IStartGameResponse> => {
    const res = await this._fetch<{
      username: string;
      game_id: string;
    }>("POST", "/start_game", { username });

    return {
      username: res["username"],
      gameID: res["game_id"],
    };
  };

  playGame = async (
    username: string,
    gameID: string,
    currentIndex: number,
    previousIndex: number
  ): Promise<IPlayGameResponse> => {
    const res = await this._fetch<{
      is_card_matched: boolean;
      round: number;
      current_card_value: number;
      opened_cards: Array<{
        card_index: number;
        card_value: number;
      }>;
    }>("POST", `/play_game/${gameID}`, {
      username,
      current_card_index: currentIndex,
      previous_card_index: previousIndex,
    });

    return {
      currentCardValue: res.current_card_value,
      isCardMatched: res.is_card_matched,
      // TODO: don't do it in realworld.
      // it's down performace when have huge data
      openedCards: res.opened_cards.map((_) => ({
        cardIndex: _.card_index,
        cardValue: _.card_value,
      })),
      round: res.round,
    };
  };

  private async _fetch<T>(
    method: string,
    endpoint: string,
    body: any = null
  ): Promise<T> {
    const fetchConfig: RequestInit = {
      method: method,
      headers: {
        "Content-Type": "application/json",
      },
    };

    if (body) {
      fetchConfig.body = JSON.stringify(body);
    }

    const res = await fetch(`${API}${endpoint}`, fetchConfig);

    if (res.status < 200 || res.status > 299) {
      throw "something went wrong";
    }

    const data = await res.json();

    return data as T;
  }
}

export class MockCardService implements CardService {
  startGame(username: string): Promise<IStartGameResponse> {
    throw new Error("Method not implemented.");
  }
  playGame(
    username: string,
    gameID: string,
    currentIndex: number,
    previousIndex: number
  ): Promise<IPlayGameResponse> {
    throw new Error("Method not implemented.");
  }
  getGlobalScore(): Promise<number> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(1000);
      }, 100);
    });
  }
  getMyScore(username: string): Promise<number> {
    throw new Error("Method not implemented.");
  }
}
