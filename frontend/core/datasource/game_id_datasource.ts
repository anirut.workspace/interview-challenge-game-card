/**
 * @deprecated
 */
export abstract class GameIDDatasource {
  abstract getGameID(): string | null;
  abstract setGameID(username: string): void;
  abstract removeGameID(): void;
}

/**
 * @deprecated
 */
export class GameIDDatasourceImpl implements GameIDDatasource {
  constructor(private readonly localStorate: Storage) {}
  removeGameID(): void {
    return this.localStorate.removeItem("game_id");
  }

  getGameID(): string | null {
    return this.localStorate.getItem("game_id");
  }
  setGameID(gameID: string): void {
    return this.localStorate.setItem("game_id", gameID);
  }
}
