import React, { useCallback, useEffect, useState } from "react";
import Card from "./components/card";
import GamePanel from "./components/panel";

import usePlayCard from "../../core/hooks/play_card";
import useGlobalScore from "../../core/hooks/global_score";
import withUsername from "../../core/hocs/with_username";
import { CardServiceImpl } from "../../core/services/card_service";
import useMyBestScore from "../../core/hooks/my_score";

function GameCard() {
  const [globalScore, isNewGlobal] = useGlobalScore();
  const [myBestScore, updateMyBestCore] = useMyBestScore() as any;
  const [gameID, setGameID] = useState<string>("");

  const [cards, round, tapCard, resetGame] = usePlayCard(async () => {
    // game done callback
    if (confirm("Do you want to new game ?")) {
      resetGame();
      newGame();
    }
  });

  const service = new CardServiceImpl();

  const newGame = useCallback(async () => {
    const username = localStorage.getItem("username");
    const gameIDLocal = localStorage.getItem("game_id");

    if (gameIDLocal) {
      resetGame();
      if (!confirm("Do you want to new game?")) {
        return;
      }
    }

    const { gameID } = await service.startGame(username as string);
    updateMyBestCore();
    setGameID(gameID);
    localStorage.setItem("game_id", gameID);
  }, []);

  return (
    <div className="game_card_container">
      <GamePanel
        gameID={gameID}
        round={round}
        myBestScore={myBestScore as number}
        globalScore={globalScore}
        isNewGlobal={isNewGlobal}
        handleTapNewGame={newGame}
        username={localStorage.getItem("username") as string}
      />
      {gameID && (
        <div className="game_card_container__cards">
          {cards.map(({ index, value }: any) => (
            <Card key={index} index={index} value={value} action={tapCard} />
          ))}
        </div>
      )}
    </div>
  );
}

export default withUsername(GameCard);
