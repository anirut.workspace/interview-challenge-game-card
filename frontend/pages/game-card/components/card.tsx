import React from "react";

const Card = ({ index, value, action }: any) => {
  function handleOnClickCard() {
    if (value) return;
    action(index);
  }

  function buildClassname() {
    return value != null ? "card card--flip" : "card";
  }

  return (
    <div onClick={handleOnClickCard} className={buildClassname()}>
      <div className="card__side card__side--front"></div>
      <div className="card__side card__side--back">{value}</div>
      <div></div>
    </div>
  );
};

export default React.memo(Card);
