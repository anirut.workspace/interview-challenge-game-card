import React from "react";

const GamePanel = ({
  round,
  globalScore,
  isNewGlobal,
  handleTapNewGame,
  username,
  myBestScore,
  gameID,
}: {
  round: number;
  isNewGlobal: boolean;
  globalScore: number;
  username: string;
  myBestScore: number;
  gameID: string;
  handleTapNewGame: Function;
}) => {
  return (
    <div className="game_card_container__control_panel">
      <div className="game_card_container__control_panel__info">
        <h1>user: {username}</h1>
        <h1>GameID: {gameID}</h1>
        <h1>Click: {round}</h1>
        <h1>My Best: {myBestScore == -1 ? "not found" : myBestScore} </h1>
        <h1
          className={`game_card_container__control_panel__info__global_score ${
            isNewGlobal
              ? "game_card_container__control_panel__info__global_score--blink"
              : ""
          }`}
        >
          Global Best: {globalScore}
        </h1>
      </div>
      <button
        className="game_card_container__button"
        onClick={(_) => handleTapNewGame()}
      >
        new game
      </button>
    </div>
  );
};

export default React.memo(GamePanel);
