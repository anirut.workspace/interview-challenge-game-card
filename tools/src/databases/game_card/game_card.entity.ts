import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { BaseEntity } from "../core/base_entity";
import { EGameHistory } from "./game_history.entity";

@Entity("game_card")
// EGameCard = Entity Game Card (table)
export class EGameCard extends BaseEntity {
  @PrimaryColumn({
    type: "uuid",
    name: "id",
  })
  id: string;

  @Column({
    name: "username",
    type: "varchar",
    length: 100,
  })
  username: string;


  @Column({
    type: "varchar",
    name: "cards",
  })
  cards: string;

  @Column({
    type: "integer",
    name: "clicktimes",
  })
  clicktimes: number;

  @OneToMany((_) => EGameHistory, (e) => e.gameCard)
  histories: EGameHistory;
}
