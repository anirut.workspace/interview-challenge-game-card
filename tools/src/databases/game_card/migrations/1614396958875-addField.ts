import {MigrationInterface, QueryRunner} from "typeorm";

export class addField1614396958875 implements MigrationInterface {
    name = 'addField1614396958875'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "openCard"`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "previousOpenCard"`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "openedCards"`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "open_card_index" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "open_card_value" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "previous_open_card_index" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "previous_open_card_value" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "opened_cards" character varying(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_card" ADD "cards" character varying NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "game_history"."created_at" IS NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ALTER COLUMN "created_at" SET DEFAULT 'now()'`);
        await queryRunner.query(`COMMENT ON COLUMN "game_card"."created_at" IS NULL`);
        await queryRunner.query(`ALTER TABLE "game_card" ALTER COLUMN "created_at" SET DEFAULT 'now()'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "game_card" ALTER COLUMN "created_at" SET DEFAULT '2021-02-26 07:56:33.608889+00'`);
        await queryRunner.query(`COMMENT ON COLUMN "game_card"."created_at" IS NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ALTER COLUMN "created_at" SET DEFAULT '2021-02-26 07:56:33.608889+00'`);
        await queryRunner.query(`COMMENT ON COLUMN "game_history"."created_at" IS NULL`);
        await queryRunner.query(`ALTER TABLE "game_card" DROP COLUMN "cards"`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "opened_cards"`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "previous_open_card_value"`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "previous_open_card_index"`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "open_card_value"`);
        await queryRunner.query(`ALTER TABLE "game_history" DROP COLUMN "open_card_index"`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "openedCards" character varying(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "previousOpenCard" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "game_history" ADD "openCard" integer NOT NULL`);
    }

}
