import { Column } from "typeorm";

export class BaseEntity {
  @Column({
    type: "timestamp with time zone",
    name: "created_at",
    default: "now()",
  })
  createdAt: Date;

  @Column({
    type: "timestamp with time zone",
    name: "updated_at",
    nullable: true,
  })
  updatedAt: Date;

  @Column({
    type: "boolean",
    name: "is_active",
    default: false,
  })
  isActive: boolean;
}
