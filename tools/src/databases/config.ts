module.exports = [
  {
    name: "game_card",
    type: "postgres",
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT as string, 10),
    database: "game_card",
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    entities: ["dist" + "/**/**/**.entity.js"],
    synchronize: false,
    migrations: ["dist/game_card/migrations/**/*.js"],
    cli: {
      migrationsDir: "src/databases/game_card/migrations/",
    },
  },
];
