from pydantic import BaseModel

class StartNewGameRequest(BaseModel):
    username: str


class PlayGameRequest(BaseModel):
    username: str
    # game_id: str
    current_card_index: int
    previous_card_index: int
