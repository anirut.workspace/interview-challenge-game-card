import psycopg2
import os
import redis
import pika
from usecase.card import CardUsecase


def declare_queues(channel, queues):
    for v in queues:
        channel.queue_declare(queue=v)

def main():
    r = redis.Redis(host=os.environ["REDIS_HOST"], port=6379, password='')

    con = psycopg2.connect(database="game_card",
                           user=os.environ["DATABASE_USERNAME"],
                           password=os.environ["DATABASE_PASSWORD"],
                           host=os.environ["DATABASE_HOST"],
                           port=int(os.environ["DATABASE_PORT"]))

    amqp = pika.BlockingConnection(
        pika.ConnectionParameters(os.environ["RABBITMQ_HOST"]))

    channel = amqp.channel()

    declare_queues(
        channel, ["new_game", "play_game", "update_user_score", "game_finish"])

    usecase = CardUsecase(channel, con, r)

    usecase.new_game_consume()

    usecase.play_game_consume()

    usecase.update_user_score_consume()

    usecase.game_finish_consume()

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    main()