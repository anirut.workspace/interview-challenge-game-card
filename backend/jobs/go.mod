module update_global_score

go 1.16

require (
	github.com/go-redis/redis/v8 v8.6.0 // indirect
	github.com/lib/pq v1.9.0 // indirect
)
